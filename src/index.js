require('dotenv').config({ path: '/app/variables/.env' });
//require('dotenv').config({ path: 'variables/.env' });

var info = require('debug')('node-api:info');
var error = require('debug')('node-api:error');

const express = require('express');
var cors = require('cors')
const morgan = require('morgan'); 
const { Server } = require("socket.io");
const app = express();
const http = require("http");
const server = http.createServer(app);
const redis = require('redis');
const helmet = require('helmet');

// Settings
app.set('port', process.env.PORT || 3500);
app.set('json spaces', 2);

// Middleware
app.use(morgan('dev'));
app.use(cors())
app.use(express.urlencoded({extended:false}));
app.use(express.json());
app.use(helmet());

// Routes
app.use(require('./routes/index'));
app.use(require('./routes/metadata'));
app.use(require('./routes/installation'));


// CORS policy
const io = new Server(server, {
    cors: {
      origin: 'https://observatory-dev.openebench.bsc.es', 
      methods: ["GET", "POST"]
    }
  })

  // Starting the server
server.listen(app.get('port'), () => {
    info(`Server on port ${app.get('port')}`);
    });


// Redis client
async function connectRedis(){

    const client = redis.createClient({
        socket: {
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT
        }
    });

    client.on('error', err => error('Redis Client Error', err));

    await client.connect();

    return client;

}

/*
The following code is used to send the installation ID to the client after
the installation of the app in a repository is complete. 
The client sends a socket event (`installation-requested`) when the user is 
redirected to the app installation page. The server stores the socket id and 
the repository name`. 
When the app is installed in the repository, the server sends the installation ID 
to the client using the socket id.

NOTE: It is here because socket.io CORS policy is configured here using the
`server` object.
*/

// pre-installation

// var requestsReposIds = {}; // stores the socket id and the repository name
// var requestsIdsRepos = {}; // stores the repository name and the socket id. This is the inverse of `requestsReposIds`
io.on('connection', async (socket) => {
    info(`New socket.io connection: ${socket.id}`);

    var client = await connectRedis();

    /* This event is sent by the client when the user is redirect to the app 
    installation page. The server stores the socket id and the repository name 
    in `requestsReposIds` and `requestsIdsRepos`.
    */
    
    socket.on('installation-requested', async (data) => {
        var repository = data['owner'].toLowerCase() + '/' + data['repo'].toLowerCase();
        await client.set(repository, socket.id);
        await client.set(socket.id, repository);

        //requestsReposIds[repository] = socket.id; // store socket id and repo
        //requestsIdsRepos[socket.id] = repository;

        info(`socket with ID ${socket.id} is waiting for installation of ${repository}`)
       
    });

    // disconnection
    /* On disconnection, delete the socket id and the repository name from the dictionaries,
    since those are no longer "active installations".
    */
    socket.on('disconnect', async (socket) => {
        info(`Socket ${socket.id} disconnected`);
        //var repository = requestsIdsRepos[socket.id];
        var repository = await client.get(socket.id);
        //var repository = requestsIdsRepos[socket.id];

        //delete requestsIdsRepos[socket.id];
        //delete requestsReposIds[repository];
        
        info(`Deleted socket ${socket.id} and repository ${repository} from dictionaries`);
    });
});


// post-installation
/* This event is a webhooks sent by GitHub when something changes in the app instalation,
like when it is installed in a repository or when it is uninstalled. 
This event is configured in the GitHub app settings (Webhook).
*/
app.post('/payloads', async  (req, res) => {
    var  data  = req.body;

    var client = await connectRedis();

    if(data.action === 'created'){
        for(var i = 0; i < data.repositories.length; i++){
            // get repository name to obtain the socket id an emit the event to the correct client
            var repository = data.repositories[i]['full_name'].toLowerCase();
            //var socketId = requestsReposIds[repository]; 
            var socketId = await client.get(repository);
            // emit event to client with the installation id
            io.to(socketId).emit('new-installation', data.installation.id); 

            info('App installed in repository ' + repository)
            info('Sent installation Id to ' + socketId)
        }
    }
    if(data.action === 'added'){
        for(var i = 0; i < data.repositories_added.length; i++){
            // get repository name to obtain the socket id an emit the event to the correct client
            var repository = data.repositories_added[i]['full_name'].toLowerCase();
            //var socketId = requestsReposIds[repository]; 
            var socketId = await client.get(repository);
            // emit event to client with the installation id
            io.to(socketId).emit('new-installation', data.installation.id); 

            info('App installed in repository ' + repository)
            info('Sent installation Id to ' + socketId)
        }
    }
});

// Test
app.get('/test', async  (req, res) => {
    data = {
       message: 'Hello World!'
    }
    info('Test')
    res.json(data);
});

// Test redis
app.get('/test-redis', async  (req, res) => {
    var client = await connectRedis();
    info('Test redis connection')
    res.json('connected to redis');
});

//require('dotenv').config({ path: 'variables/.env' });
require('dotenv').config({ path: '/app/variables/.env' });

const { Octokit, App  } = require('octokit');

module.exports = {

    authApp: async function () {
        // Authenticate as an installation of an app
        // Returns an authenticated instance of octokit

        // reading the app private key from a file
        const appID = process.env.APP_ID;
        const fs = require("fs");
        
        // if developing locally, use the following line:
        //var myKey = fs.readFileSync("variables/oeb-fairsoft-evaluator.2023-05-29.private-key.pem", "utf8");
        
        // if running in docker, use the following line instead:
        const privateKeyPath = process.env.PRIVATE_KEY_PATH;
        var myKey = fs.readFileSync(privateKeyPath, "utf8");
        
        //console.log(`appID: ${appID}`)
        //console.log(`myKey: ${myKey}`)
        const app = await new App({
            appId: appID,
            privateKey: myKey,
        });

        return app;

    }

}


const { Router, json } = require('express');
const router = Router();
var  { personalAccessToken , authApp }  = require('./auth');
var info = require('debug')('node-api:info');
var error = require('debug')('node-api:error');


/* ---------------------------------- */
/*          POST /metadata            */
/* -----------------------------------*/

async function queryRepositoryObject(octokit, owner, repo){
    // Use the Explorer to build the GraphQL query: https://docs.github.com/en/graphql/overview/explorer
    const { repository } = await octokit.graphql(
        `
        {    
            repository(owner: "${owner}", name: "${repo}") {
            
            collaborators(first: 10) {
                nodes {
                    email
                    name
                }
            }
            description
            descriptionHTML
            homepageUrl
            isDisabled
            isEmpty
            isFork
            isInOrganization
            isLocked
            isMirror
            isPrivate
            isTemplate
            latestRelease {
                name
                tagName
            }
            licenseInfo {
                id
                name
                spdxId
                url
            }
            name
            mirrorUrl
            packages(first: 10) {
                nodes {
                    id
                    name
                    packageType
                    version(version: "") {
                    version
                    summary
                    }
                }
            }
            releases(last: 10) {
                nodes {
                    id
                    tagName
                    name
                    url
                }
            }
            url
            repositoryTopics(first: 10) {
                nodes {
                url
                topic {
                    id
                    name
                }
                }
            }
            }
        }
    `
    );

    // transform data to the observatory metadata schema
    return repository;

}

function removeNull(array){
    return array.filter(val => val !== null)
}

function buildTopics(githubObject){
    /*
    For the license in the github object, generate an item in the topics array
    {
        "vocabulary": "EDAM",
        "term": "Topic",
        "uri": "http://edamontology.org/topic_0003"
    }
    */
    let topics = [];
    if(githubObject.repositoryTopics.nodes.length > 0){
        githubObject.repositoryTopics.nodes.forEach((node) => {
            var item = {
                uri: node.url,
                term: node.topic.name,
                vocabulary: ''
            }
            topics.push(item);
        });
    }
    
    return topics;
}

function buildAuthors(githubObject){
    /*
    For each collaborator in the github object, generate an object in the authors array
    {
        "name": "John Doe",
        "email": "",
        "maintainer": "true",
    }
    */
    var authors = [];
    // for each collaborator, generate an author object
    if(githubObject.collaborators.nodes.length > 0){
        for( var i = 0; i < githubObject.collaborators.nodes.length; i++){
            var item = {
                name: githubObject.collaborators.nodes[i].name,
                // github collaborator type is always person
                type: 'person',
                email: githubObject.collaborators.nodes[i].email,
                maintainer: false
            }
            authors.push(item);
        }
    }
    
    return authors;
}


function buildLicense(githubObject){
    /*
    For each license in the github object, generate an object in the license array
    {
        "name": "MIT License",
        "url" : "https://opensource.org/licenses/MIT",
    }
    */
    if(githubObject.licenseInfo){
        var licenses = [{
            name: githubObject.licenseInfo.name,
            url: githubObject.licenseInfo.url
        }]
    }else{
        var licenses = [];
    }
    return licenses;
}

function githubMetadata(ghObject){
    const meta = {
        name: ghObject.name,
        label: [
            ghObject.name
        ],
        description: removeNull([ 
            ghObject.description 
        ]),
        links: removeNull([
            ghObject.mirrorUrl 
        ]),
        webpage: removeNull([
            ghObject.homepageUrl
        ]),
        isDisabled: ghObject.isDisabled,
        isEmpty: ghObject.isEmpty,
        isLocked: ghObject.isLocked,
        isPrivate: ghObject.isPrivate,
        isTemplate: ghObject.isTemplate,
        version: ghObject.releases.nodes.map((node) => node.tagName),
        license: buildLicense(ghObject),
        repository: removeNull([ 
            ghObject.url
        ]),
        
        topics: buildTopics(ghObject),
        operations: [],
        authors: buildAuthors(ghObject),
        bioschemas: false,
        contribPolicy: [],
        dependencies: [],
        documentation: [],
        download: [], // This could be package or come from repository contents
        edam_operations: [],
        edam_topics: [],
        https: true,
        input: [],
        inst_instr: false,
        operational: false,
        os: [],
        output: [],
        publication: [],
        semantics: {
            inputs: [],
            outputs: [],
            topics: [],
            operations: [],
        },
        source: ['github'],
        src: [],
        ssl: true,
        tags: [],
        test: [],
        type: "",   
    }

    return meta;

}
    
function PrepareListsIds(metadata){
    /*
    For each field in the metadata, if it is a list, add an id to each item in the list
    From:
    [
        term1,
        term2,
        ...
    ]
    To:
    [
        { term: term1, id: id1 },
        { term: term2, id: id2 },
        ...
    ]
    */
    const fields = [
        'edam_topics',
        'edam_operations',
        'documentation',
        'description',
        'webpage',
        'license',
        'src',
        'links',
        'topics',
        'operations',
        'input',
        'output',
        'repository',
        'dependencies',
        'os',
        'authors',
        'publication',
    ]
    
    for (const field of fields) {
        var n=0;
        new_field = []
        for (var item of metadata[field]) {
            new_field.push({
                term: item,
                id: n
            });
            n++;
        };
        metadata[field] = new_field;
    }
    return metadata;

}

async function getRepositoryMetadata(octokit, owner, repo){
    // Use the Explorer to build the GraphQL query: https://docs.github.com/en/graphql/overview/explorer
    const repository = await queryRepositoryObject(octokit, owner, repo);
    info('Repository object retrieved. Transforming to metadata')
    var metadata = githubMetadata(repository); // transform data to the observatory metadata schema
    metadata = PrepareListsIds(metadata); // add ids to specific lists so they can be used in the UI
    info('Metadata transformed. Returning metadata')
    return metadata;
}

// Get metadata using app authentication and installation ID
router.post('/metadata', (req, res) => { 
    const { owner, repo, installationID } = req.body;
    info('Authenticating app')
    authApp().then((app) => {
        info('App authenticated. Getting installation octokit')
        info('Installation Id: ' + installationID)
        app.getInstallationOctokit(installationID).then((octokit) => {
            console.log('Installation octokit retrieved. Getting repository metadata')
            getRepositoryMetadata(octokit, owner, repo).then((data) => {
            console.log(data)
            res.json({
                data: data,
                status: 200,
            });
            }).catch((error) => {
                res.json({
                    data: null,
                    status: error.status,
                    message: error.message,
                });
            });
        });
    });

});


/* ------------------------------------------- */
/*           POST /metadata/pull               */
/* --------------------------------------------*/

async function getOctokit(installationID){
    /*
    Get an octokit instance using app authentication
    */
    const app = await authApp();
    const octokit = await app.getInstallationOctokit(installationID);
    return octokit;
}


function jsonToBase64(object) {
    /*
    Transform a JSON object to a base64 string
    */
    const json = JSON.stringify(object);
    return Buffer.from(json).toString("base64");
  }

async function getSHAofMaster(octokit, owner, repo){
    /*
    Get the SHA of the master branch
    Get all branches and look for the one named master or main
    Returns the SHA of the master branch, the name of the branch and all the branch names
    */
    const resp = await octokit.request('GET /repos/{owner}/{repo}/branches', {
        owner: owner,
        repo: repo,
    })

    const branches = resp.data;
    const all_branch_names = branches.map((branch) => branch.name);
    for (const branch of branches) {
        if (branch.name == 'master' || branch.name == 'main'){
            info('SHA of master branch: ' + branch.commit.sha)
            return { 
                masterSHA : branch.commit.sha,
                branchName : branch.name,
                allBranchNames : all_branch_names
        };
        }
    }
    return null;
  }

function generateBranchName(branches){
    // look for 'evaluator' and 'evaluator-n' branches.
    const re = new RegExp("^evaluator(-[0-9]+)?$");
    const evaluator_branches = branches.allBranchNames.filter((branch) => re.test(branch));
    if(evaluator_branches.length != 0){
        // get number group
        const re2 = new RegExp("^evaluator(-([0-9]+))?$");
        var numbers = evaluator_branches.map((branch) => re2.exec(branch)[2]);
        // remove nulls
        numbers = numbers.filter((number) => number != undefined);
        // select biggest number
        const max_number = Math.max(...numbers);
        // add 1
        const new_number = max_number + 1;
        return 'evaluator-' + new_number;
    }else{
        return 'evaluator-1';
    }
}

async function createBranch(octokit, owner, repo, branchName, sha){
    /*
    Create a new branch from master
    ref: ref of the new branch. Example: refs/heads/my-new-branch
    sha: SHA of the commit to branch from (master/main)
    */

    console.log('createBranch')

    const resp = await octokit.request('POST https://api.github.com/repos/{owner}/{repo}/git/refs',{
        owner: owner,
        repo: repo,
        ref: "refs/heads/" + branchName,
        sha: sha, 
    })
    return resp;
}

async function createFile(octokit, owner, repo, branchName, path, content, message) {
    /*
    Create a new file in a branch
    */
    console.log('createFile');
    console.log(owner, repo, path);

    let contents;

    try {
        contents = await octokit.request('GET /repos/{owner}/{repo}/contents/{path}', {
            owner: owner,
            repo: repo,
            path: path,
        });
    } catch (error) {
        if (error.status === 404) {
            // File does not exist, so proceed to create it
            contents = null;
        } else {
            // Some other error occurred
            throw error;
        }
    }

    if (contents && contents.status === 200) {
        // File already exists, update the file
        const resp = await octokit.request('PUT /repos/{owner}/{repo}/contents/{path}', {
            owner: owner,
            repo: repo,
            path: path,
            sha: contents.data.sha,
            branch: branchName,
            message: message,
            committer: {
                name: 'OEB FAIRsoftEvaluator',
                email: 'openebench@bsc.es'
            },
            content: content // The new file content, using Base64 encoding. https://docs.github.com/en/rest/repos/contents?apiVersion=2022-11-28#create-or-update-file-contents
        });

        return resp;
    } else {
        // File does not exist, create it
        const resp = await octokit.request('PUT /repos/{owner}/{repo}/contents/{path}', {
            owner: owner,
            repo: repo,
            path: path,
            branch: branchName,
            message: message,
            committer: {
                name: 'OEB FAIRsoftEvaluator',
                email: 'openebench@bsc.es'
            },
            content: content // The new file content, using Base64 encoding. https://docs.github.com/en/rest/repos/contents?apiVersion=2022-11-28#create-or-update-file-contents
        });

        return resp;
    }
}


async function createPullRequest(octokit, owner, repo, head, base, title, body){
    /*
    Create a new pull request
    */
   console.log('createPullRequest')
    const resp = await octokit.request('POST /repos/{owner}/{repo}/pulls',{
        owner: owner,
        repo: repo,
        title: title,
        head: head,
        base: base,
        body: body,
        accept: 'application/vnd.github+json'
    })
    return resp;
};


router.post('/metadata/pull', async (req, res) => {
    /* 
    This endpoint creates a new pull request with the codemeta file. It does:
    1. Get content to add.
    2. Get SHA of master branch (name: master or main).
    3. Create a new branch from master.
    4. Add files to branch and commit:
    5. Create pull request
    */

    const { owner, repo, filename, installationID, metadata, title, message } = req.body;

    // Set default values for title and message if not provided
    const defaultMessage = `Description of this software (\`${filename}\`) generated by [OEB FAIR Evaluator](https://github.com/apps/oeb-fairsoft-evaluator) added.`;
    const defaultTitle = `Metadata for this software`;

    const finalTitle = title || defaultTitle;
    const finalMessage = message || defaultMessage;


    try{
        const content = jsonToBase64(metadata);
        
        const octokit = await getOctokit(installationID);

        const branches = await getSHAofMaster(octokit, owner, repo);
        const sha = branches.masterSHA;
        const baseBranch = branches.branchName;
        const newBranchName = generateBranchName(branches)
    
        await createBranch(octokit, owner, repo, newBranchName, sha);

        console.log(metadata)
        

        //octokit, owner, repo, branchName, path, content, message
        await createFile(octokit, owner, repo, newBranchName, filename, content, finalMessage);
        console.log('file created')
        
        
        const pullrequest = await createPullRequest(octokit, 
            owner, 
            repo, 
            newBranchName, 
            baseBranch, 
            finalTitle,
            finalMessage
            );

        resp = {
            status: 200,
            code: 200,
            message: 'success',
            new_branch_name: newBranchName,
            head_branch_name: baseBranch,
            url: pullrequest.data.html_url,
            pullrequest_message: pullrequest
            }

    }catch(e){
        error(e);
        resp = {
            code: e.status,
            message: e.message
        }
        
    } finally {
        res.send(resp);
    }
    
})

/* ------------------------------------------- */
/*           POST /metadata/content             */
/* --------------------------------------------*/

router.post('/metadata/content', async (req, res) => {
    /*
    This endpoint gets the content of a file in a repository.
    path: path to the file in the repository
    */
    console.log(req.body)

    const { owner, repo, path, installationID } = req.body;

    console.log(owner, repo, path, installationID)

    try{
 
        const app = await authApp();
        console.log('authApp done')
        const octokit = await app.getInstallationOctokit(installationID);
        console.log('getInstallationOctokit done')

        const response = await octokit.request('GET /repos/{owner}/{repo}/contents/{path}', {
            owner: owner,
            repo: repo,
            path: path,
            accept : 'application/vnd.github+json'
          })
        
        console.log('request done')
        console.log(response.data)
        const file_content = Buffer.from(response.data.content, 'base64').toString('utf8');
        const json = JSON.parse(file_content);

        resp = {
            status: 200,
            code: 200,
            message: 'success',
            content: json
            }

    }catch(e){
        error(e);
        console.log(e)
        resp = {
            status: e.status,
            code: e.code,
            message: e.message
        }
        
    } finally {
        res.send(resp);
    }
})

module.exports = router;

/* Only for testing purposes

module.exports = githubMetadata; 
*/



